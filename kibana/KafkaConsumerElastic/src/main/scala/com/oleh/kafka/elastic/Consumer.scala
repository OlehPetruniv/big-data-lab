package com.oleh.kafka.elastic

import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._

import scala.collection.mutable
import scala.io.Source

object Consumer {

  val censored = "*censored*"
  var banList = new mutable.HashSet[String]()
  val MAX_CENSORED_WORDS = 7

  val messageFilter = "\\{(\"(speaker|time|word)\":\"((\\w+)|(\\d{4}-\\d{2}-\\d{2}\\d{2}:\\d{2}:\\d{2}.\\d))\",?){3}\\}"

  val SPLIT_SEPARATOR = "\""

  def main(args: Array[String]): Unit = {

    val spark = SparkSession.builder()
      .appName("Message Consumer")
      .master("local[*]")
      .config("spark.es.nodes","127.0.0.1")
      .config("spark.es.port","9200")
      .getOrCreate()

      val BROKER_HOST = args(0)
      val TOPIC = args(1)
      val FILE_PATH = args(2)
      val WINDOW_DURATION_MIN = args(3)

     val censoredWords = Source.fromFile(FILE_PATH).getLines().toList

    Logger.getLogger("org").setLevel(Level.OFF)
    Logger.getLogger("akka").setLevel(Level.OFF)

    val data = spark.readStream
      .format("kafka")
      .option("kafka.bootstrap.servers", BROKER_HOST)
      .option("subscribe", TOPIC)
      .load()


    import spark.implicits._

    val message = data.selectExpr("cast(value as string) as value")
      .withColumn("value", regexp_replace(col("value"), "\\s+", ""))
      .filter(col("value").rlike(messageFilter))
      .as[String].map(s => {

      val list = s.split(SPLIT_SEPARATOR).toList

      val speaker = findVal("speaker", list)
      var time = findVal("time", list)
      val word = findVal("word", list)
      // make empty space between date and time

      time = time.substring(0, 10) + " " + time.substring(10)

      (speaker, time, word)

    }).withColumnRenamed("_1", "speaker")
      .withColumnRenamed("_2", "time")
      .withColumnRenamed("_3", "word")
      .as[Message]


    val censoredMessage = message.map(m => {
      if (censoredWords.contains(m.word)) {
        (m.speaker, m.time, censored)
      } else {
        (m.speaker, m.time, m.word)
      }
    }).filter(m => !banList.contains(m._1))
      .withColumnRenamed("_1", "speaker")
      .withColumnRenamed("_2", "time")
      .withColumnRenamed("_3", "word").as[Message]


    val withEventTime = censoredMessage.selectExpr("*", "cast(time as timestamp) as event_time")
      .withWatermark("event_time", "2 minutes")

    val resTable = withEventTime.groupBy(window(col("event_time"), WINDOW_DURATION_MIN + " minutes"),
      col("speaker"))
      .agg(concat_ws(", ", collect_list(col("word"))))
      .withColumnRenamed("concat_ws(, , collect_list(word))", "words")

    resTable
      .writeStream
      .option("truncate", "false")
      .format("console")
      .outputMode("complete")
      .start()



    //this would be saved to elasticsearch
    withEventTime.writeStream
      .format("org.elasticsearch.spark.sql")
      .outputMode("append")
      .option("checkpointLocation", "D:\\check-1")
      .start("kafka-messages-1/message")



    val censoredCount = censoredMessage.filter(m => m.word == censored).groupBy(col("speaker"))
      .count()
      .withColumnRenamed("count", "censored_count")
      .as[CensoredCount]


    censoredCount.map(m => {
      if (m.censored_count >= MAX_CENSORED_WORDS) {
        banList.add(m.speaker)
      }
      m
    }).writeStream
      .option("truncate", "false")
      .format("console")
      .outputMode("complete")
      .start()

    spark.streams.awaitAnyTermination()


  }

  def findVal(key: String, list: List[String]): String = {
    var value = ""

    for (i <- 1 until list.length) {
      if (list(i).equals(key)) {
        value = list(i + 2)
      }
    }

    value
  }


  case class Message(speaker: String, time: String, word: String)



  case class CensoredCount(speaker: String, censored_count: Long)

}
