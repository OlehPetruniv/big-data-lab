package com.oleh.producer

import java.time.LocalDateTime
import java.util.Properties

import org.apache.kafka.clients.producer.{KafkaProducer, ProducerConfig, ProducerRecord}


import scala.io.Source
import scala.util.Random

object Producer {

  var startTime = LocalDateTime.now();
  var messageId = LocalDateTime.now().getNano;
  val MESSAGES_TO_PRODUCE = 15;
  val WRONG_MESSAGES = 10;

  def main(args: Array[String]): Unit = {
    val properties = new Properties()

    val FILE_PATH = args(3)
    val TOPIC = args(1)
    val SPEAKER_ARR = args(2).split(',')
    val BROKER_HOST = args(0)

    val words = Source.fromFile(FILE_PATH).getLines().toList
    val speakers = SPEAKER_ARR.map(s => new Speaker(name = s, startTime = startTime))

    properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, BROKER_HOST)
    properties.put(ProducerConfig.CLIENT_ID_CONFIG, "Message Producer")
    properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer")
    properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer")

    val producer = new KafkaProducer[String, String](properties)

    val correctMassages = new Thread(new Runnable {
      override def run(): Unit =
        for (i <- 0 until MESSAGES_TO_PRODUCE) {
          val message = generateMessage(speakers, words).toString();
          messageId+=1
          val data = new ProducerRecord[String, String](TOPIC, messageId + "", message)
          producer.send(data)
          Thread.sleep(1000)
        }
    })

    val wrongMessages = new Thread(new Runnable {
      override def run(): Unit =
        for (i <- 0 until WRONG_MESSAGES) {
          messageId+=1
          val newData = new ProducerRecord[String, String](TOPIC, messageId +"", "it`s wrong data")
          producer.send(newData)
          Thread.sleep(1000)
        }
    })

    correctMassages.start()
    wrongMessages.start()
    correctMassages.join()
    wrongMessages.join()

    producer.close()

  }


  def generateMessage(speakers: Array[Speaker], words: List[String]): Message = {
    val speaker = speakers(Random.nextInt(speakers.length))
    Message(speaker = speaker.getName(), time = speaker.getTime(), word = words(Random.nextInt(words.length)))
  }

  case class Message(speaker: String, time: String, word: String) {
    override def toString() = "{\n" + "\t\"speaker\": \"" + speaker + "\",\n" + "\t\"time\": \"" + time + "\"," +
      "\n" + "\t\"word\": \"" + word + "\"\n}"
  }

}
