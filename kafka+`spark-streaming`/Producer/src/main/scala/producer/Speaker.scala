package com.oleh.producer

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class Speaker(name:String, startTime:LocalDateTime) {

  var lastMessageTime = startTime;
  val format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S")

  def getName():String={
    name
  }

  def getTime():String ={
    val time = lastMessageTime.format(format)
    lastMessageTime = lastMessageTime.plusMinutes(scala.util.Random.nextInt(5))
    lastMessageTime = lastMessageTime.plusSeconds(scala.util.Random.nextInt(60))
    time
  }


}
