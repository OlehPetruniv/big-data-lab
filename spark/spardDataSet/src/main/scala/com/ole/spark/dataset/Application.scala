package com.ole.spark.dataset

import org.apache.spark.sql.{Dataset, SparkSession}

object Application {

  def main(args: Array[String]): Unit = {

            val masterPath = args(0)
            val scoringPath = args(1)
            val teamsPath = args(2)
            val awardsPath = args(3)
            val resultPath = args(4)


    val spark = SparkSession.builder.master("local[*]")
      .appName("Hockey Players DataSet")
      .getOrCreate()

    import spark.implicits._

    // read files as data frames
    val master = spark.read.option("header", "true")
      .format("csv").load(masterPath)
      .select("playerID", "firstName", "lastName")
      .as[Master]

    val awards = spark.read.option("header", "true")
      .format("csv").load(awardsPath)
      .select("playerID", "award")
      .as[Awards]

    val team = spark.read.option("header", "true")
      .format("csv").load(teamsPath)
      .select("tmID", "name")
      .as[Team]

    val scoring = spark.read.option("header", "true")
      .format("csv").load(scoringPath)
      .select("playerID", "tmID", "G")
      .as[Scoring]


    val playerGoals = scoring.map(g => {
      var goals = 0;
      if (g.G != null && g.G != "") {
        goals = g.G.toInt
      }
      (g.playerID, goals)
    }).groupByKey(_._1).reduceGroups((a, b) => (a._1, a._2 + b._2)).map(_._2)
      .withColumnRenamed("_1", "playerId").withColumnRenamed("_2", "goals")
      .as[PlayerGoals]


    /// join all data together

    val playerTeams = scoring.joinWith(team, scoring.col("tmID") === team.col("tmID"))
      .map(d => (d._1.playerID, d._2.name))
      .distinct()
      .groupByKey(_._1)
      .reduceGroups((a, b) => (a._1, a._2 + ", " + b._2)).map(_._2)
      .withColumnRenamed("_1", "playerId").withColumnRenamed("_2", "teams")
      .as[PlayerTeams]


    val playerNameAwards = master.joinWith(awards, master.col("playerID") === awards.col("playerID"), "left")
      .map(d => {
        if (d._2!=null) {
          (d._1, 1)
        }
        else {
          (d._1, 0)
        }
      }).groupByKey(_._1.playerID).reduceGroups((a, b) => (a._1, a._2 + b._2)).map(_._2)
      .map(m => (m._1.playerID, m._1.firstName + " " + m._1.lastName, m._2))
      .withColumnRenamed("_1", "playerID")
      .withColumnRenamed("_2", "name")
      .withColumnRenamed("_3", "awards")
      .as[NameAwards]



    val pNameAwGoals = playerNameAwards
      .joinWith(playerGoals, playerGoals.col("playerID") === playerNameAwards.col("playerID"))
      .map(m => (m._1.playerId, m._1.name, m._1.awards, m._2.goals))
      .withColumnRenamed("_1", "playerID")
      .withColumnRenamed("_2", "name")
      .withColumnRenamed("_3", "awards")
      .withColumnRenamed("_4", "goals")
      .as[NameAwardsGoals]


    val allData =
      pNameAwGoals.joinWith(playerTeams, playerTeams.col("playerID") === pNameAwGoals.col("playerID"))
        .map(all => (all._1.name, all._1.awards, all._1.goals, all._2.teams))

    val result = allData.orderBy(allData.col("_3").desc).limit(10)
      .withColumnRenamed("_1", "name")
      .withColumnRenamed("_2", "awards")
      .withColumnRenamed("_3", "goals")
      .withColumnRenamed("_4", "teams")

    result.write.option("header", "true")
      .csv(resultPath)


    spark.stop()

  }

  case class NameAwardsGoals(playerId: String, name: String, awards: Int, goals: Int)

  case class NameAwards(playerId: String, name: String, awards: Int)

  case class Result(firstName: String, lastName: String, awards: Int, goals: Int, teams: String)

  case class PlayerGoals(playerId: String, goals: Int)

  case class PlayerTeams(playerId: String, teams: String)

  case class Master(playerID: String, firstName: String, lastName: String)

  case class Scoring(playerID: String, tmID: String, G: String)

  case class Awards(playerID: String, award: String)

  case class Team(tmID: String, name: String)

}
