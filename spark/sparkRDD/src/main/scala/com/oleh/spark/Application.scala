package com.oleh.spark

import com.oleh.spark.entity._
import org.apache.spark.{SparkConf, SparkContext}

object Application {


  def main(args: Array[String]): Unit = {


    val conf = new SparkConf()
      .setAppName("Hockey_RDD")
//      .setMaster("local[*]");

    val sc = new SparkContext(conf);

    val masterPath = args(0)
    val scoringPath = args(1)
    val teamsPath = args(2)
    val awardsPath = args(3)
    val resultPath = args(4)


    // pId + player full name
    val masterRDDfull = sc.textFile(masterPath).filter(!_.startsWith("playerID"))
    val masterRDD = MasterData.getOnlyImportant(masterRDDfull)

    // pId + goals
    val scroaringRDDfull = sc.textFile(scoringPath).filter(!_.startsWith("playerID"))
    val scoringRDD = ScoringData.getOnlyImportant(scroaringRDDfull)

    // teamId + team name
    val teamsRDDfull = sc.textFile(teamsPath).filter(!_.startsWith("year"))
    val teamsRDD = TeamsData.getOnlyImportant(teamsRDDfull)

    // pId + awardsCount
    val awardsRDDfull = sc.textFile(awardsPath).filter(!_.startsWith("playerID"))
    val awardsRDD = AwardsData.getOnlyImportant(awardsRDDfull).map(a => (a.pId, 1)).reduceByKey(_ + _)

    // pId + teamId
    val playerTeamRDD = PlayerTeamData.getOnlyImportant(scroaringRDDfull, teamsRDD)

    // map rdd`s for future joining
    val mPT = playerTeamRDD.map(pt => (pt.tId, pt.pId))
    val mT = teamsRDD.map(t => (t.tId, t.tName))

    // making rdd with player id and all teams it
    val pIdTeamName = mPT.join(mT).map(pt => (pt._2._1, pt._2._2)).distinct().reduceByKey(_ + ", " + _)



    val mappedM = masterRDD.map(m => (m.pId, m.fullName))
    val mappedG = scoringRDD.map(g => (g.pId, g.goals))
    val mappedA = awardsRDD.map(a => (a._1, a._2))

    val full = mappedM.join(mappedG).join(pIdTeamName).leftOuterJoin(mappedA)
      .map(f => new OutData(f._2._1._1._1, f._2._2, f._2._1._1._2, f._2._1._2))
      .sortBy(out => out.goals, false, 1)
      .map(data => {
        new String(data.name + " | " + data.awards.getOrElse(0) + " | " + data.goals + " | " + data.teams)
      })

    val top10 = sc.parallelize(full.take(10))

    val header = sc.parallelize(Array("name | awards | goals | teams")).repartition(1)

    val result = header.union(top10).repartition(1)

    result.saveAsTextFile(resultPath)

    sc.stop();
  }


  case class OutData(name: String, awards: Option[Int], goals: Int, teams: String)

}
