package com.oleh.spark.entity

import org.apache.spark.rdd.RDD


object ScoringData {

  def getOnlyImportant(scoringRDD: RDD[String]):RDD[Goalies] = {
    scoringRDD.map(s => {
      val line = s.replaceAll(",",",_")
      val fields: Array[String] = line.split(",");
      val first = getValue(fields(7));
      new Goalies(fields(0), first)
    }).map(g => (g.pId, g.goals)).reduceByKey(_+_)
      .map(g => new Goalies(g._1, g._2))
  }

  def getValue(value: String): Int ={
    var result: Int = 0;
    if (value != "_"){
      result = value.substring(1).toInt
    }
    result
  }

  case class Goalies(pId: String, goals: Int);
}
