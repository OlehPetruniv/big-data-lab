package com.oleh.spark.entity

import com.oleh.spark.entity.TeamsData.Team
import org.apache.spark.rdd.RDD

object PlayerTeamData {

  def getOnlyImportant(scoringRDD: RDD[String], teamsRDD: RDD[Team]):RDD[PlayerTeam] = {
    scoringRDD.map(s => {
      val line = s.replaceAll(",",",_")
      val fields: Array[String] = line.split(",");

      new PlayerTeam(fields(0), fields(3).substring(1))
    })
  }


    case class PlayerTeam(pId:String, tId:String)
}
