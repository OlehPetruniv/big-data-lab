package com.oleh.spark.entity

import org.apache.spark.rdd.RDD

object TeamsData {

  def getOnlyImportant(teamRDD: RDD[String]):RDD[Team]={
    teamRDD.map(s => {
      val line = s.replaceAll(",",",_")
      val fields: Array[String] = line.split(",")
      new Team(fields(2).substring(1), fields(18).substring(1))
    })
  }


  case class Team(tId: String, tName:String)
}
