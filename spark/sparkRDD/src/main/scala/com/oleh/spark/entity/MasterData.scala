package com.oleh.spark.entity

import org.apache.spark.rdd.RDD


object MasterData {


  def getOnlyImportant(masterRDD: RDD[String]): RDD[Player] = {
    masterRDD.map(line => {
      val fields: Array[String] = line.split(",");
     new Player(fields(0), fields(3) +" " + fields(4))
    })
  }


  case class Player(pId: String, fullName: String)

}
