package com.oleh.spark.entity

import org.apache.spark.rdd.RDD

object AwardsData {

  def getOnlyImportant(awardsRDD: RDD[String]): RDD[Awards] = {
    awardsRDD.map(line => {
      val fields: Array[String] = line.split(",");
      new Awards(fields(0), fields(1))
    })
  }

  case class Awards(pId: String, awards: String)
}
