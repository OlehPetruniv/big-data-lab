spark-submit --class com.ole.spark.dataset.Application --master yarn --deploy-mode client sparddataset_2.11-0.1.jar /data/Master.csv /data/Scoring.csv /data/Teams.csv /data/AwardsPlayers.csv /spark_res/dataset_res

spark-submit --class com.oleh.spark.Application --master yarn --deploy-mode client sparkrdd_2.11-0.1.jar /data/Master.csv /data/Scoring.csv /data/Teams.csv /data/AwardsPlayers.csv /spark_res/rdd_res

spark-submit --class com.oleh.spark.sql.Application --master yarn --deploy-mode client sparksql_2.11-0.1.jar /data/Master.csv /data/Scoring.csv /data/Teams.csv /data/AwardsPlayers.csv /spark_res/sql_res