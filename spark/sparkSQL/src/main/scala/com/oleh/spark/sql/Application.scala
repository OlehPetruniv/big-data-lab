package com.oleh.spark.sql

import org.apache.spark.sql.SparkSession

object Application {
  def main(args: Array[String]): Unit = {

    val spark = SparkSession.builder.appName("Hockey Players")
      .master("local[*]")
      .getOrCreate();

        val masterPath = args(0)
        val scoringPath = args(1)
        val teamsPath = args(2)
        val awardsPath = args(3)
        val resultPath = args(4)

    spark.read.format("csv").option("header", "true")
      .load(masterPath)
      .createOrReplaceTempView("master");

    spark.read.format("csv").option("header", "true")
      .load(scoringPath)
      .createOrReplaceTempView("scoring");

    spark.read.format("csv").option("header", "true")
      .load(teamsPath)
      .createOrReplaceTempView("teams");

    spark.read.format("csv").option("header", "true")
      .load(awardsPath)
      .createOrReplaceTempView("awards");


    spark.sql("select playerId,  cast(sum(G) as int) as goals from scoring group by playerId")
      .createOrReplaceTempView("playerGoals")


    spark.sql("select playerId, CONCAT_WS(' ',firstName, lastName) as pName from master")
      .createOrReplaceTempView("playerName")

    spark.sql("select playerId, count(award) as awards_ctn from awards group by playerId")
      .createOrReplaceTempView("playerAward")



    // player team table creating
    spark.sql("select distinct scoring.playerId, teams.name " +
      "from scoring join teams on scoring.tmID = teams.tmID")
      .createOrReplaceTempView("pIDteamName")


    spark.sql("select playerId, CONCAT_WS(', ', collect_list(name)) as teams" +
      " from pIDteamName group by playerID")
      .createOrReplaceTempView("playerTeams")

    spark.sql("select playerName.playerId, playerName.pName, playerGoals.goals" +
      " from playerName join playerGoals on playerName.playerId = playerGoals.playerId")
      .createOrReplaceTempView("nameGoals")

    spark.sql("select nameGoals.playerId, nameGoals.pName, playerAward.awards_ctn, nameGoals.goals" +
      " from nameGoals left join playerAward on nameGoals.playerId = playerAward.playerId")
      .createOrReplaceTempView("nameAwardsGoals")


    val result = spark.sql("select nameAwardsGoals.pName as name, cast(nameAwardsGoals.awards_ctn as int) as awards, nameAwardsGoals.goals," +
      " playerTeams.teams from nameAwardsGoals join playerTeams " +
      "on nameAwardsGoals.playerId=playerTeams.playerId " +
      "order by nameAwardsGoals.goals desc limit 10")

    import spark.implicits._

   result.map(r=>{
     var award = 0;
     var tableAward = r.getAs[Int]("awards")
     if (tableAward!=null){
       award = tableAward;
     }
     (r.getAs[String]("name"), award,r.getAs[Int]("goals"),
     r.getAs[String]("teams"))
   }).withColumnRenamed("_1", "name")
     .withColumnRenamed("_2", "awards")
     .withColumnRenamed("_3", "goals")
     .withColumnRenamed("_4", "teams").write.option("header", "true").csv(resultPath)

    spark.stop()
  }
}
