package com.oleh.secondarysort;

import org.apache.avro.Schema;
import org.apache.avro.generic.GenericData;
import org.apache.avro.mapred.AvroKey;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.types.Pair;
import org.junit.Before;
import org.junit.Test;



public class HotelMapperTest {

    private static final Schema AVRO_SCHEMA = new Schema.Parser().parse(
            "{\n" +
                    "  \"type\" : \"record\",\n" +
                    "  \"name\" : \"topLevelRecord\",\n" +
                    "  \"fields\" : [ {\n" +
                    "    \"name\" : \"id\",\n" +
                    "    \"type\" : [ \"long\", \"null\" ]\n" +
                    "  }, {\n" +
                    "    \"name\" : \"date_time\",\n" +
                    "    \"type\" : [ \"string\", \"null\" ]\n" +
                    "  }, {\n" +
                    "    \"name\" : \"site_name\",\n" +
                    "    \"type\" : [ \"int\", \"null\" ]\n" +
                    "  }, {\n" +
                    "    \"name\" : \"posa_continent\",\n" +
                    "    \"type\" : [ \"int\", \"null\" ]\n" +
                    "  }, {\n" +
                    "    \"name\" : \"user_location_country\",\n" +
                    "    \"type\" : [ \"int\", \"null\" ]\n" +
                    "  }, {\n" +
                    "    \"name\" : \"user_location_region\",\n" +
                    "    \"type\" : [ \"int\", \"null\" ]\n" +
                    "  }, {\n" +
                    "    \"name\" : \"user_location_city\",\n" +
                    "    \"type\" : [ \"int\", \"null\" ]\n" +
                    "  }, {\n" +
                    "    \"name\" : \"orig_destination_distance\",\n" +
                    "    \"type\" : [ \"double\", \"null\" ]\n" +
                    "  }, {\n" +
                    "    \"name\" : \"user_id\",\n" +
                    "    \"type\" : [ \"int\", \"null\" ]\n" +
                    "  }, {\n" +
                    "    \"name\" : \"is_mobile\",\n" +
                    "    \"type\" : [ \"int\", \"null\" ]\n" +
                    "  }, {\n" +
                    "    \"name\" : \"is_package\",\n" +
                    "    \"type\" : [ \"int\", \"null\" ]\n" +
                    "  }, {\n" +
                    "    \"name\" : \"channel\",\n" +
                    "    \"type\" : [ \"int\", \"null\" ]\n" +
                    "  }, {\n" +
                    "    \"name\" : \"srch_ci\",\n" +
                    "    \"type\" : [ \"string\", \"null\" ]\n" +
                    "  }, {\n" +
                    "    \"name\" : \"srch_co\",\n" +
                    "    \"type\" : [ \"string\", \"null\" ]\n" +
                    "  }, {\n" +
                    "    \"name\" : \"srch_adults_cnt\",\n" +
                    "    \"type\" : [ \"int\", \"null\" ]\n" +
                    "  }, {\n" +
                    "    \"name\" : \"srch_children_cnt\",\n" +
                    "    \"type\" : [ \"int\", \"null\" ]\n" +
                    "  }, {\n" +
                    "    \"name\" : \"srch_rm_cnt\",\n" +
                    "    \"type\" : [ \"int\", \"null\" ]\n" +
                    "  }, {\n" +
                    "    \"name\" : \"srch_destination_id\",\n" +
                    "    \"type\" : [ \"int\", \"null\" ]\n" +
                    "  }, {\n" +
                    "    \"name\" : \"srch_destination_type_id\",\n" +
                    "    \"type\" : [ \"int\", \"null\" ]\n" +
                    "  }, {\n" +
                    "    \"name\" : \"hotel_id\",\n" +
                    "    \"type\" : [ \"long\", \"null\" ]\n" +
                    "  } ]\n" +
                    "}\n"
    );


    AvroKey<GenericData.Record> key;
    MapDriver<AvroKey<GenericData.Record>, NullWritable, CompositeKey, IntWritable> driver;


    @Before
    public void setUp(){
        driver = new MapDriver<>();
        GenericData.Record record = new GenericData.Record(AVRO_SCHEMA);
        key = new AvroKey<>(record);
        key.datum().put("srch_adults_cnt", 4);
        key.datum().put("srch_ci","2016-08-03");
        key.datum().put("channel",8);
        key.datum().put("id",1241234234L);
        key.datum().put("hotel_id",895445456L);
    }


    @Test
    public void testHotelMapper(){
        NullWritable value = NullWritable.get();
        driver.withMapper(new HotelMapper())
                .withInput(key, value)
                .withOutput(new
                        Pair<>(new CompositeKey(4,1241234234L,"2016-08-03", 895445456L),
                        new IntWritable(8)))
                .runTest();

    }

}
