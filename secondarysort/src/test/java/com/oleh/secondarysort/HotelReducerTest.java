package com.oleh.secondarysort;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.apache.hadoop.mrunit.types.Pair;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class HotelReducerTest {

    ReduceDriver<CompositeKey, IntWritable, CompositeKey, IntWritable> driver;
    CompositeKey key;
    IntWritable value;
    List<IntWritable> values;

    @Before
    public void setUp(){
        key = new CompositeKey(4,1241234234L,"2016-08-03", 895445456L);
        driver = new ReduceDriver<>();
        value = new IntWritable(8);
        values = new ArrayList<>();
        values.add(value);
    }

    @Test
    public void testHotelReducer(){

        driver.withReducer(new HotelReducer())
                .withInput(key, values)
                .withOutput(new Pair<>(key, value))
                .runTest();

    }

}
