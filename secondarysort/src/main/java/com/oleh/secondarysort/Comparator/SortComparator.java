package com.oleh.secondarysort.Comparator;

import com.oleh.secondarysort.CompositeKey;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

public class SortComparator extends WritableComparator {
    public SortComparator() {
        super(CompositeKey.class, true);
    }

    @Override
    public int compare(WritableComparable a, WritableComparable b) {
        CompositeKey first = (CompositeKey) a;
        CompositeKey second = (CompositeKey) b;

        int res = first.getHotelId().compareTo(second.getHotelId());
        if (res != 0) {
            return res;
        }
        res = first.getSrchCi().compareTo(second.getSrchCi());
        if (res != 0) {
            return res;
        }
        return first.getId().compareTo(second.getId());

    }
}
