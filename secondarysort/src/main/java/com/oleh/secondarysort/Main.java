package com.oleh.secondarysort;

import com.oleh.secondarysort.Comparator.GroupComparator;
import com.oleh.secondarysort.Comparator.SortComparator;
import org.apache.avro.Schema;
import org.apache.avro.mapreduce.AvroJob;
import org.apache.avro.mapreduce.AvroKeyInputFormat;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class Main extends Configured implements Tool {

    private static final Schema AVRO_SCHEMA = new Schema.Parser().parse(
            "{\n" +
                    "  \"type\" : \"record\",\n" +
                    "  \"name\" : \"topLevelRecord\",\n" +
                    "  \"fields\" : [ {\n" +
                    "    \"name\" : \"id\",\n" +
                    "    \"type\" : [ \"long\", \"null\" ]\n" +
                    "  }, {\n" +
                    "    \"name\" : \"date_time\",\n" +
                    "    \"type\" : [ \"string\", \"null\" ]\n" +
                    "  }, {\n" +
                    "    \"name\" : \"site_name\",\n" +
                    "    \"type\" : [ \"int\", \"null\" ]\n" +
                    "  }, {\n" +
                    "    \"name\" : \"posa_continent\",\n" +
                    "    \"type\" : [ \"int\", \"null\" ]\n" +
                    "  }, {\n" +
                    "    \"name\" : \"user_location_country\",\n" +
                    "    \"type\" : [ \"int\", \"null\" ]\n" +
                    "  }, {\n" +
                    "    \"name\" : \"user_location_region\",\n" +
                    "    \"type\" : [ \"int\", \"null\" ]\n" +
                    "  }, {\n" +
                    "    \"name\" : \"user_location_city\",\n" +
                    "    \"type\" : [ \"int\", \"null\" ]\n" +
                    "  }, {\n" +
                    "    \"name\" : \"orig_destination_distance\",\n" +
                    "    \"type\" : [ \"double\", \"null\" ]\n" +
                    "  }, {\n" +
                    "    \"name\" : \"user_id\",\n" +
                    "    \"type\" : [ \"int\", \"null\" ]\n" +
                    "  }, {\n" +
                    "    \"name\" : \"is_mobile\",\n" +
                    "    \"type\" : [ \"int\", \"null\" ]\n" +
                    "  }, {\n" +
                    "    \"name\" : \"is_package\",\n" +
                    "    \"type\" : [ \"int\", \"null\" ]\n" +
                    "  }, {\n" +
                    "    \"name\" : \"channel\",\n" +
                    "    \"type\" : [ \"int\", \"null\" ]\n" +
                    "  }, {\n" +
                    "    \"name\" : \"srch_ci\",\n" +
                    "    \"type\" : [ \"string\", \"null\" ]\n" +
                    "  }, {\n" +
                    "    \"name\" : \"srch_co\",\n" +
                    "    \"type\" : [ \"string\", \"null\" ]\n" +
                    "  }, {\n" +
                    "    \"name\" : \"srch_adults_cnt\",\n" +
                    "    \"type\" : [ \"int\", \"null\" ]\n" +
                    "  }, {\n" +
                    "    \"name\" : \"srch_children_cnt\",\n" +
                    "    \"type\" : [ \"int\", \"null\" ]\n" +
                    "  }, {\n" +
                    "    \"name\" : \"srch_rm_cnt\",\n" +
                    "    \"type\" : [ \"int\", \"null\" ]\n" +
                    "  }, {\n" +
                    "    \"name\" : \"srch_destination_id\",\n" +
                    "    \"type\" : [ \"int\", \"null\" ]\n" +
                    "  }, {\n" +
                    "    \"name\" : \"srch_destination_type_id\",\n" +
                    "    \"type\" : [ \"int\", \"null\" ]\n" +
                    "  }, {\n" +
                    "    \"name\" : \"hotel_id\",\n" +
                    "    \"type\" : [ \"long\", \"null\" ]\n" +
                    "  } ]\n" +
                    "}\n"
    );


    public int run(String[] strings) throws Exception {
        if (strings.length!=2){
            System.err.println("Usage: secondary-sort.jar <input path> <output path>");
            return -1;
        }

        Configuration conf = new Configuration();
        Job job = new Job();
        job.setJarByClass(Main.class);
        job.setJobName("Secondary sort");

        FileInputFormat.setInputPaths(job, new Path(strings[0]));
        FileOutputFormat.setOutputPath(job, new Path(strings[1]));

        job.setInputFormatClass(AvroKeyInputFormat.class);
        job.setMapperClass(HotelMapper.class);

        AvroJob.setInputKeySchema(job, AVRO_SCHEMA);
        job.setGroupingComparatorClass(GroupComparator.class);
        job.setPartitionerClass(HotelPartioner.class);
        job.setNumReduceTasks(2);

        job.setSortComparatorClass(SortComparator.class);
        job.setGroupingComparatorClass(GroupComparator.class);

        job.setOutputKeyClass(CompositeKey.class);
        job.setOutputValueClass(IntWritable.class);
        job.setReducerClass(HotelReducer.class);
        Path outputPath = new Path(strings[1]);
        outputPath.getFileSystem(conf).delete(outputPath, true);
        return (job.waitForCompletion(true) ? 0 : 1);

    }

    public static void main(String[] args) {
        try {
            int res = ToolRunner.run(new Main(), args);
            System.out.println(res);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
