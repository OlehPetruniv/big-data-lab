package com.oleh.secondarysort;

import org.apache.avro.generic.GenericData;
import org.apache.avro.mapred.AvroKey;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class HotelMapper extends Mapper<AvroKey<GenericData.Record>, NullWritable, CompositeKey, IntWritable> {


    @Override
    protected void map(AvroKey<GenericData.Record> key, NullWritable value, Context context) throws IOException, InterruptedException {
        Integer srchAdultsCnt = (Integer) key.datum().get("srch_adults_cnt");
        CharSequence srch_ci_str = (CharSequence) key.datum().get("srch_ci");
        if (srch_ci_str==null){
            srch_ci_str = "no data";
        }
        Integer channel_int = (Integer) key.datum().get("channel");
        IntWritable channel = new IntWritable(channel_int);
        Long id = (Long) key.datum().get("id");
        Long hotel_id = (Long) key.datum().get("hotel_id");
        context.write(new CompositeKey(srchAdultsCnt, id, srch_ci_str.toString(), hotel_id), channel);


    }
}
