package com.oleh.secondarysort;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class HotelReducer extends Reducer<CompositeKey, IntWritable, CompositeKey, IntWritable> {

    @Override
    protected void reduce(CompositeKey key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
        for (IntWritable channel : values) {
            context.write(key, channel);
        }
    }
}
