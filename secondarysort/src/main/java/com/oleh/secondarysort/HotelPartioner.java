package com.oleh.secondarysort;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Partitioner;

public class HotelPartioner extends Partitioner<CompositeKey, IntWritable> {

    @Override
    public int getPartition(CompositeKey compositeKey, IntWritable intWritable, int i) {
        if (compositeKey.getAmountOfAdults()>2) {
            return 1%i;
        }
        else{
            return 2%i;
        }
    }
}
