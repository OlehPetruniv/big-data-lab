package com.oleh.secondarysort;

import org.apache.hadoop.io.WritableComparable;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Objects;

public class CompositeKey implements WritableComparable<CompositeKey> {


    private Integer amountOfAdults;
    private Long id;
    private String srchCi;
    private Long hotelId;



    public CompositeKey() {
        this(0, 0L, new String(), 0L);
    }

    public CompositeKey(Integer amountOfAdults, Long id, String srchCi, Long hotelId) {
        this.amountOfAdults = amountOfAdults;
        this.id = id;
        if (srchCi == null) {
            this.srchCi = "0000-00-00";
        } else {
            this.srchCi = srchCi;
        }
        this.hotelId = hotelId;
    }

    @Override
    public int compareTo(CompositeKey o) {

        int res = this.hotelId.compareTo(o.getHotelId());
        if (res == 0) {
            res = this.srchCi.compareTo(o.getSrchCi());

        }
        if (res == 0) {
            res = this.id.compareTo(o.getId());
        }


        return res;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CompositeKey that = (CompositeKey) o;
        return Objects.equals(amountOfAdults, that.amountOfAdults) &&
                Objects.equals(id, that.id) &&
                Objects.equals(srchCi, that.srchCi) &&
                Objects.equals(hotelId, that.hotelId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(amountOfAdults, id, srchCi, hotelId);
    }

    public Long getId() {
        return id;
    }

    public String getSrchCi() {
        return srchCi;
    }

    public Long getHotelId() {
        return hotelId;
    }

    public Integer getAmountOfAdults() {
        return amountOfAdults;
    }

    @Override
    public void readFields(DataInput dataInput) throws IOException {
        amountOfAdults = dataInput.readInt();
        id = dataInput.readLong();
        srchCi = dataInput.readUTF();
        hotelId = dataInput.readLong();

    }

    @Override
    public void write(DataOutput dataOutput) throws IOException {

        dataOutput.writeInt(amountOfAdults);
        dataOutput.writeLong(id);
        dataOutput.writeUTF(srchCi);
        dataOutput.writeLong(hotelId);

    }

    @Override
    public String toString() {
        return "CompositeKey{" +
                "amountOfAdults=" + amountOfAdults +
                ", id=" + id +
                ", srchCi='" + srchCi + '\'' +
                ", hotelId=" + hotelId +
                '}';
    }
}
